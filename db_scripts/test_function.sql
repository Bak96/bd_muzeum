drop function if exists dniPozaMuzeum(integer, integer) cascade;
drop function if exists czyWolnyTermin(integer, date, date) cascade;
drop function if exists normArtysci() cascade;
drop function if exists normEksponaty() cascade;
drop function if exists normInstytucje() cascade;
drop function if exists normGalerie() cascade;
drop function if exists normWystObj() cascade;
drop function if exists poprawnoscWypozyczeniaInstytucjom() cascade;
drop function if exists poprawnoscWypozyczeniaGaleriom () cascade;
drop function if exists poprawnoscWypozyczeniaWystawomObj () cascade;
drop function if exists czyscArtysci () cascade;



/*id_eksponatu, data*/
CREATE FUNCTION dniPozaMuzeum(integer, integer) RETURNS integer as $$
	DECLARE
		ileWInst integer;
		ileWObjazdach integer;
	BEGIN
		SELECT sum((data_do - data_od + 1)) INTO ileWInst FROM ekspwinst
		WHERE id_eksp = $1 AND $2 = date_part('year', data_do)
		GROUP BY id_eksp;
		
		SELECT sum((w.data_do - w.data_od + 1)) INTO ileWObjazdach FROM ekspnawystobj e
		JOIN wystobj w ON e.id_wyst = w.id_wyst
		WHERE e.id_eksp = $1 AND $2 = date_part('year', w.data_do)
		GROUP BY id_eksp;
		
		IF (ileWinst IS NULL) THEN
			ileWinst = 0;
		END IF;
		
		IF (ileWObjazdach IS NULL) THEN
			ileWObjazdach = 0;
		END IF;
		
		RETURN ileWObjazdach + ileWInst;
	END;
$$ LANGUAGE 'plpgsql';

/*id_eksponatu, data_od, data_do */
CREATE FUNCTION czyWolnyTermin(integer, date, date) RETURNS integer as $$
	DECLARE
		gal integer;
		inst integer;
		obj integer;
	BEGIN
	
		IF $3 < $2 THEN
			RAISE EXCEPTION 'data_do <= data_od';
		END IF;
	
		SELECT count(*) INTO gal FROM ekspwgal
		WHERE $1 = id_eksp AND $2 <= data_do AND $3 >= data_od;
		
		SELECT count(*) INTO inst FROM ekspwinst
		WHERE $1 = id_eksp AND $2 <= data_do AND $3 >= data_od;
		
		SELECT count(*) INTO obj FROM ekspnawystobj e
		JOIN wystobj w ON e.id_wyst = w.id_wyst
		WHERE $1 = e.id_eksp AND $2 <= w.data_do AND $3 >= w.data_od;
		
		IF (gal + inst + obj) != 0 THEN
			RETURN 0;
		ELSE
			RETURN 1;
		END IF;
	END;

$$ LANGUAGE 'plpgsql';

CREATE FUNCTION normArtysci () RETURNS TRIGGER AS $$
BEGIN
	NEW.imie := upper(NEW.imie);
	NEW.nazwisko := upper(NEW.nazwisko);
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION normEksponaty () RETURNS TRIGGER AS $$
BEGIN
	NEW.tytul := upper(NEW.tytul);
	NEW.typ := upper(NEW.typ);
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION normInstytucje () RETURNS TRIGGER AS $$
BEGIN
	NEW.nazwa := upper(NEW.nazwa);
	NEW.miasto := upper(NEW.miasto);
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION normGalerie () RETURNS TRIGGER AS $$
BEGIN
	NEW.nazwa := upper(NEW.nazwa);
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION normWystObj () RETURNS TRIGGER AS $$
BEGIN
	NEW.miasto := upper(NEW.miasto);
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION poprawnoscWypozyczeniaGaleriom () RETURNS TRIGGER AS $$
DECLARE
	wolny integer;
	rok integer;
	rok1 integer;
	rok2 integer;
	
BEGIN
	SELECT date_part('year', NEW.data_od) INTO rok1;
	SELECT date_part('year', NEW.data_do) INTO rok2;
	
	IF (rok1 != rok2) THEN
		RAISE EXCEPTION 'Można wypożyczac eksponaty tylko w obrębie jednego roku';
	END IF;
	
	SELECT czyWolnyTermin(NEW.id_eksp, NEW.data_od, NEW.data_do) INTO wolny;
	SELECT date_part('year', NEW.data_od) INTO rok;
	
	IF (wolny = 0) THEN
		RAISE EXCEPTION 'Termin jest już zajęty';
	END IF;
	
	RETURN NEW;
END;

$$ LANGUAGE 'plpgsql';

CREATE FUNCTION poprawnoscWypozyczeniaWystawomObj () RETURNS TRIGGER AS $$
DECLARE
	dane record;
	ileDni integer;
	wolny integer;
	rok integer;
	rok1 integer;
	rok2 integer;
	
BEGIN
	select * into dane from wystobj
	where id_wyst = NEW.id_wyst;
	
	SELECT date_part('year', dane.data_od) INTO rok1;
	SELECT date_part('year', dane.data_do) INTO rok2;
	
	IF (rok1 != rok2) THEN
		RAISE EXCEPTION 'Można wypożyczac eksponaty tylko w obrębie jednego roku';
	END IF;
	
	SELECT czyWolnyTermin(NEW.id_eksp, dane.data_od, dane.data_do) INTO wolny;
	SELECT date_part('year', dane.data_od) INTO rok;
	SELECT dniPozaMuzeum(NEW.id_eksp, rok) INTO ileDni;
	
	IF (wolny = 0) THEN
		RAISE EXCEPTION 'Termin jest już zajęty';
	END IF;
	
	IF (ileDni + (dane.data_do - dane.data_od + 1) > 30) THEN
		RAISE EXCEPTION 'Eksponat nie może być poza muzeum więcej niż 30 dni w roku';
	END IF;
	
	RETURN NEW;
END;

$$ LANGUAGE 'plpgsql';

CREATE FUNCTION poprawnoscWypozyczeniaInstytucjom () RETURNS TRIGGER AS $$
DECLARE
	ileDni integer;
	wolny integer;
	rok integer;
	rok1 integer;
	rok2 integer;
	
BEGIN
	
	SELECT date_part('year', NEW.data_od) INTO rok1;
	SELECT date_part('year', NEW.data_do) INTO rok2;
	
	IF (rok1 != rok2) THEN
		RAISE EXCEPTION 'Można wypożyczac eksponaty tylko w obrębie jednego roku';
	END IF;
	
	SELECT czyWolnyTermin(NEW.id_eksp, NEW.data_od, NEW.data_do) INTO wolny;
	SELECT date_part('year', NEW.data_od) INTO rok;
	SELECT dniPozaMuzeum(NEW.id_eksp, rok) INTO ileDni;
	
	IF (wolny = 0) THEN
		RAISE EXCEPTION 'Termin jest już zajęty';
	END IF;
	
	IF (ileDni + (NEW.data_do - NEW.data_od + 1) > 30) THEN
		RAISE EXCEPTION 'Eksponat nie może być poza muzeum więcej niż 30 dni w roku';
	END IF;
	
	RETURN NEW;
END;

$$ LANGUAGE 'plpgsql';

CREATE FUNCTION czyscArtysci () RETURNS TRIGGER AS $$
DECLARE
	ileEksponatow integer;
BEGIN
	SELECT count(*) INTO ileEksponatow FROM Eksponaty
	WHERE id_art = OLD.id_art;
	
	IF (ileEksponatow = 0) THEN
		DELETE FROM Artysci WHERE id_art = OLD.id_art;
	END IF;

	RETURN OLD;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER artysciTrigger
	BEFORE INSERT OR UPDATE ON Artysci
	FOR EACH ROW
	EXECUTE PROCEDURE normArtysci();

CREATE TRIGGER eksponatyTrigger
	BEFORE INSERT OR UPDATE ON Eksponaty
	FOR EACH ROW
	EXECUTE PROCEDURE normEksponaty();

CREATE TRIGGER instytucjeTrigger
	BEFORE INSERT OR UPDATE ON Instytucje
	FOR EACH ROW
	EXECUTE PROCEDURE normInstytucje();
	
CREATE TRIGGER galerieTrigger
	BEFORE INSERT OR UPDATE ON Galerie
	FOR EACH ROW
	EXECUTE PROCEDURE normGalerie();
	
CREATE TRIGGER wystObjTrigger
	BEFORE INSERT OR UPDATE ON WystObj
	FOR EACH ROW
	EXECUTE PROCEDURE normWystObj();

CREATE TRIGGER ekspWInstTrigger
	BEFORE INSERT OR UPDATE ON ekspWInst
	FOR EACH ROW
	EXECUTE PROCEDURE poprawnoscWypozyczeniaInstytucjom();
	
CREATE TRIGGER ekspWGalTrigger
	BEFORE INSERT OR UPDATE ON ekspWGal
	FOR EACH ROW
	EXECUTE PROCEDURE poprawnoscWypozyczeniaGaleriom();
	
CREATE TRIGGER ekspNaWystObjTrigger
	BEFORE INSERT OR UPDATE ON ekspNaWystObj
	FOR EACH ROW
	EXECUTE PROCEDURE poprawnoscWypozyczeniaWystawomObj();

CREATE TRIGGER czyscArtysciTrigger
	AFTER UPDATE OR DELETE ON Eksponaty
	FOR EACH ROW
	EXECUTE PROCEDURE czyscArtysci();


