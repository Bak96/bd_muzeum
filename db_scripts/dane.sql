INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('jan', 'matejko', 1838, 1893);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('pablo', 'picasso', 1881, 1973);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('michał', 'anioł', 1475, 1564);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('paweł', 'pawłowski', 1990, NULL);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('jezus', 'chrystus', 0, 33);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('Józef', 'Malina', 1880 , 1930);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('Piotr', 'Mnich', 1996 , NULL);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('Grzegorz', 'Myśliciel', 1990 , NULL);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('Tadeusz', 'Krawiec', 1939 , 1989);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('Grażyna', 'Ząbek', 1960 , NULL);


INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Stworzenie Pana', 1, 'obraz', 10, 9, 8, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Matka Boska', 3, 'rzezba', 5, 5, 800, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Magiczny Kubek', 5, 'przedmiot', 1, 2, 3, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Zaczarowany olowek', 2, 'przedmiot', 4, 3, 2, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Kapelusz ciemnosci', 6, 'przedmiot', 0.1, 0.2, 1, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Kraina zła', 9, 'obraz', 10, 10, 1, 'N');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Zielony kwadrat', 6, 'obraz', 11, 10, 1, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Obszary nędzy', 5, 'obraz', 5, 4, 1, 'N');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Heros', 9, 'rzezba', 10, 3.5, 80, 'N');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 1');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 2');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 3');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 4');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 5');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 6');


INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (1, '10-12-2014', '11-12-2014', 5, 1);

INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (1, '10-12-2014', '11-12-2014', 4, 1);

INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (1, '14-12-2014', '16-12-2014', 1, 1);

INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (2, '14-12-2014', '16-12-2014', 3, 2);

INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (2, '17-12-2014', '20-12-2014', 5, 3);

INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (2, '01-02-2017', '20-02-2017', 5, 3);

INSERT INTO wystobj (miasto, data_od, data_do)
VALUES ('warszawa', '10-12-2014', '15-12-2014');

INSERT INTO wystobj (miasto, data_od, data_do)
VALUES ('katowice', '10-05-2015', '15-05-2015');

INSERT INTO wystobj (miasto, data_od, data_do)
VALUES ('przyłubie', '17-12-2014', '20-12-2014');

INSERT INTO wystobj (miasto, data_od, data_do)
VALUES ('poznań', '01-01-2015', '09-01-2015');

INSERT INTO wystobj (miasto, data_od, data_do)
VALUES ('gdańsk', '01-02-2016', '09-02-2016');

INSERT INTO ekspnawystobj(id_wyst, id_eksp)
VALUES (1, 2);

INSERT INTO ekspnawystobj(id_wyst, id_eksp)
VALUES (1, 8);

INSERT INTO ekspnawystobj(id_wyst, id_eksp)
VALUES (4, 1);


INSERT INTO instytucje(nazwa, miasto)
VALUES ('Intytucja Zakonnic', 'Rzeszów');

INSERT INTO instytucje(nazwa, miasto)
VALUES ('Intytucja Mieszka', 'Radom');

INSERT INTO instytucje(nazwa, miasto)
VALUES ('Galeria Obcych', 'Berlin');

INSERT INTO instytucje(nazwa, miasto)
VALUES ('Galeria Obcych', 'Berlin');

INSERT INTO instytucje(nazwa, miasto)
VALUES ('Kolorowe Kredki', 'Gdańsk');

INSERT INTO ekspwinst(id_inst, data_od, data_do, id_eksp)
VALUES (2, '05-08-2016', '09-08-2016', 5);

INSERT INTO ekspwinst(id_inst, data_od, data_do, id_eksp)
VALUES (2, '05-08-2017', '09-08-2017', 3);

INSERT INTO ekspwinst(id_inst, data_od, data_do, id_eksp)
VALUES (3, '01-02-2017', '03-02-2017', 9);

INSERT INTO uzytkownicy(username, password) 
VALUES ('bolek', 'lolek');
