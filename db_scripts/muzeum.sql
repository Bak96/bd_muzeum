drop table if exists Eksponaty cascade;
drop table if exists Magazyn cascade;
drop table if exists Artysci cascade;
drop table if exists EkspWInst cascade;
drop table if exists Instytucje cascade;
drop table if exists EkspWGal cascade;
drop table if exists Galerie cascade;
drop table if exists EkspNaWystObj cascade;
drop table if exists WystObj cascade;
drop table if exists Uzytkownicy cascade;


create table Artysci (
	id_art serial primary key not null,
	imie varchar(20) not null,
	nazwisko varchar(20) not null,
	rok_urodzenia integer,
	rok_smierci integer
);

create table Eksponaty (
	id_eksp serial primary key not null,
	tytul varchar(50) not null,
	id_art integer references Artysci,
	typ varchar(50),
	wysokosc numeric(6, 2),
	szerokosc numeric(6, 2),
	waga numeric(6,2),
	mozna_wyp char(1) check (mozna_wyp = 'T' OR mozna_wyp = 'N')
);

create table Instytucje(
	id_inst serial primary key not null,
	nazwa varchar(50) not null,
	miasto varchar(20) not null
);

create table EkspWInst(
	id_inst integer references Instytucje not null,
	data_od date not null,
	data_do date not null,
	id_eksp integer references Eksponaty not null
);

create table EkspWGal (
	id_gal integer,
	data_od date not null,
	data_do date not null,
	id_eksp integer,
	sala integer not null
);

create table Galerie (
	id_gal serial primary key not null,
	nazwa varchar(50) not null
);

create table WystObj(
	id_wyst serial primary key not null,
	miasto varchar(20),
	data_od date not null,
	data_do date not null
);

create table EkspNaWystObj(
	id_wyst integer references WystObj not null,
	id_eksp integer references Eksponaty not null
);

create table Uzytkownicy(
	username varchar(20) primary key not null,
	password varchar(20) not null
);

drop function if exists dniPozaMuzeum(integer, integer) cascade;
drop function if exists czyWolnyTermin(integer, date, date) cascade;
drop function if exists normArtysci() cascade;
drop function if exists normEksponaty() cascade;
drop function if exists normInstytucje() cascade;
drop function if exists normGalerie() cascade;
drop function if exists normWystObj() cascade;
drop function if exists poprawnoscWypozyczeniaInstytucjom() cascade;
drop function if exists poprawnoscWypozyczeniaGaleriom () cascade;
drop function if exists poprawnoscWypozyczeniaWystawomObj () cascade;
drop function if exists czyscArtysci () cascade;
drop function if exists ileEksponatowArtystyZostalo(integer, date, date) cascade;


/*id_eksponatu, data*/
CREATE FUNCTION dniPozaMuzeum(integer, integer) RETURNS integer as $$
	DECLARE
		ileWInst integer;
		ileWObjazdach integer;
	BEGIN
		SELECT sum((data_do - data_od + 1)) INTO ileWInst FROM ekspwinst
		WHERE id_eksp = $1 AND $2 = date_part('year', data_do)
		GROUP BY id_eksp;
		
		SELECT sum((w.data_do - w.data_od + 1)) INTO ileWObjazdach FROM ekspnawystobj e
		JOIN wystobj w ON e.id_wyst = w.id_wyst
		WHERE e.id_eksp = $1 AND $2 = date_part('year', w.data_do)
		GROUP BY id_eksp;
		
		IF (ileWinst IS NULL) THEN
			ileWinst = 0;
		END IF;
		
		IF (ileWObjazdach IS NULL) THEN
			ileWObjazdach = 0;
		END IF;
		
		RETURN ileWObjazdach + ileWInst;
	END;
$$ LANGUAGE 'plpgsql';

/*id_eksponatu, data_od, data_do */
CREATE FUNCTION czyWolnyTermin(integer, date, date) RETURNS integer as $$
	DECLARE
		gal integer;
		inst integer;
		obj integer;
	BEGIN
	
		IF $3 < $2 THEN
			RAISE EXCEPTION 'data_do <= data_od';
		END IF;
	
		SELECT count(*) INTO gal FROM ekspwgal
		WHERE $1 = id_eksp AND $2 <= data_do AND $3 >= data_od;
		
		SELECT count(*) INTO inst FROM ekspwinst
		WHERE $1 = id_eksp AND $2 <= data_do AND $3 >= data_od;
		
		SELECT count(*) INTO obj FROM ekspnawystobj e
		JOIN wystobj w ON e.id_wyst = w.id_wyst
		WHERE $1 = e.id_eksp AND $2 <= w.data_do AND $3 >= w.data_od;
		
		IF (gal + inst + obj) != 0 THEN
			RETURN 0;
		ELSE
			RETURN 1;
		END IF;
	END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION ileEksponatowArtystyZostalo(integer, date, date) RETURNS integer as $$
	DECLARE
		wszystkie integer;
		inst integer;
		obj integer;
	BEGIN
	
		IF $3 < $2 THEN
			RAISE EXCEPTION 'data_do <= data_od';
		END IF;
		
		SELECT count(*) INTO wszystkie FROM eksponaty
		WHERE id_art = $1;
		
		
		SELECT count(DISTINCT e1.id_eksp) INTO inst FROM ekspwinst e1
		JOIN eksponaty e2 ON e1.id_eksp = e2.id_eksp 
		WHERE $1 = e2.id_art AND $2 <= data_do AND $3 >= data_od;
		
		
		SELECT count(DISTINCT e.id_eksp) INTO obj FROM ekspnawystobj e
		JOIN wystobj w ON e.id_wyst = w.id_wyst
		JOIN eksponaty e2 ON e.id_eksp = e2.id_eksp
		WHERE $1 = e2.id_art AND $2 <= w.data_do AND $3 >= w.data_od;
		
		RETURN wszystkie - (inst + obj);
	END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION normArtysci () RETURNS TRIGGER AS $$
BEGIN
	NEW.imie := upper(NEW.imie);
	NEW.nazwisko := upper(NEW.nazwisko);
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION normEksponaty () RETURNS TRIGGER AS $$
BEGIN
	NEW.tytul := upper(NEW.tytul);
	NEW.typ := upper(NEW.typ);
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION normInstytucje () RETURNS TRIGGER AS $$
BEGIN
	NEW.nazwa := upper(NEW.nazwa);
	NEW.miasto := upper(NEW.miasto);
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION normGalerie () RETURNS TRIGGER AS $$
BEGIN
	NEW.nazwa := upper(NEW.nazwa);
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION normWystObj () RETURNS TRIGGER AS $$
BEGIN
	NEW.miasto := upper(NEW.miasto);
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE FUNCTION poprawnoscWypozyczeniaGaleriom () RETURNS TRIGGER AS $$
DECLARE
	wolny integer;
	rok integer;
	rok1 integer;
	rok2 integer;
	
BEGIN
	SELECT date_part('year', NEW.data_od) INTO rok1;
	SELECT date_part('year', NEW.data_do) INTO rok2;
	
	IF (rok1 != rok2) THEN
		RAISE EXCEPTION 'Można wypożyczac eksponaty tylko w obrębie jednego roku';
	END IF;
	
	SELECT czyWolnyTermin(NEW.id_eksp, NEW.data_od, NEW.data_do) INTO wolny;
	SELECT date_part('year', NEW.data_od) INTO rok;
	
	IF (wolny = 0) THEN
		RAISE EXCEPTION 'Termin jest już zajęty';
	END IF;
	
	RETURN NEW;
END;

$$ LANGUAGE 'plpgsql';

CREATE FUNCTION poprawnoscWypozyczeniaWystawomObj () RETURNS TRIGGER AS $$
DECLARE
	dane record;
	ileDni integer;
	wolny integer;
	rok integer;
	rok1 integer;
	rok2 integer;
	eksponatyArtysty integer;
	artysta integer;
BEGIN
	select * into dane from wystobj
	where id_wyst = NEW.id_wyst;
	
	SELECT date_part('year', dane.data_od) INTO rok1;
	SELECT date_part('year', dane.data_do) INTO rok2;
	
	IF (rok1 != rok2) THEN
		RAISE EXCEPTION 'Można wypożyczac eksponaty tylko w obrębie jednego roku';
	END IF;
	
	SELECT czyWolnyTermin(NEW.id_eksp, dane.data_od, dane.data_do) INTO wolny;
	SELECT date_part('year', dane.data_od) INTO rok;
	SELECT dniPozaMuzeum(NEW.id_eksp, rok) INTO ileDni;
	
	IF (wolny = 0) THEN
		RAISE EXCEPTION 'Termin jest już zajęty';
	END IF;
	
	SELECT id_art INTO artysta FROM Eksponaty
	WHERE id_eksp = NEW.id_eksp;
	
	IF (artysta IS NULL) THEN
		RAISE EXCEPTION 'Nie można wypożyczać eksponatów, dla których artysta jest nieznany';
	END IF;
	
	SELECT ileEksponatowArtystyZostalo(artysta, dane.data_od, dane.data_do) INTO eksponatyArtysty;
	IF (eksponatyArtysty = 1) THEN
		RAISE EXCEPTION 'Nie można wypożyczyć tego eksponatu, bo to ostatni egzemplarz podanego artysty';
	END IF;
	
	
	IF (ileDni + (dane.data_do - dane.data_od + 1) > 30) THEN
		RAISE EXCEPTION 'Eksponat nie może być poza muzeum więcej niż 30 dni w roku';
	END IF;
	
	RETURN NEW;
END;

$$ LANGUAGE 'plpgsql';

CREATE FUNCTION poprawnoscWypozyczeniaInstytucjom () RETURNS TRIGGER AS $$
DECLARE
	ileDni integer;
	wolny integer;
	rok integer;
	rok1 integer;
	rok2 integer;
	artysta integer;
	eksponatyArtysty integer;
BEGIN
	
	SELECT date_part('year', NEW.data_od) INTO rok1;
	SELECT date_part('year', NEW.data_do) INTO rok2;
	
	IF (rok1 != rok2) THEN
		RAISE EXCEPTION 'Można wypożyczac eksponaty tylko w obrębie jednego roku';
	END IF;
	
	SELECT czyWolnyTermin(NEW.id_eksp, NEW.data_od, NEW.data_do) INTO wolny;
	SELECT date_part('year', NEW.data_od) INTO rok;
	SELECT dniPozaMuzeum(NEW.id_eksp, rok) INTO ileDni;
	
	IF (wolny = 0) THEN
		RAISE EXCEPTION 'Termin jest już zajęty';
	END IF;
	
	IF (ileDni + (NEW.data_do - NEW.data_od + 1) > 30) THEN
		RAISE EXCEPTION 'Eksponat nie może być poza muzeum więcej niż 30 dni w roku';
	END IF;
	
	SELECT id_art INTO artysta FROM Eksponaty
	WHERE id_eksp = NEW.id_eksp;
	
	IF (artysta IS NULL) THEN
		RAISE EXCEPTION 'Nie można wypożyczać eksponatów, dla których artysta jest nieznany';
	END IF;
	
	SELECT ileEksponatowArtystyZostalo(artysta, NEW.data_od, NEW.data_do) INTO eksponatyArtysty;
	IF (eksponatyArtysty = 1) THEN
		RAISE EXCEPTION 'Nie można wypożyczyć tego eksponatu, bo to ostatni egzemplarz podanego artysty';
	END IF;
	
	RETURN NEW;
END;

$$ LANGUAGE 'plpgsql';

CREATE FUNCTION czyscArtysci () RETURNS TRIGGER AS $$
DECLARE
	ileEksponatow integer;
BEGIN
	SELECT count(*) INTO ileEksponatow FROM Eksponaty
	WHERE id_art = OLD.id_art;
	
	IF (ileEksponatow = 0) THEN
		DELETE FROM Artysci WHERE id_art = OLD.id_art;
	END IF;

	RETURN OLD;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER artysciTrigger
	BEFORE INSERT OR UPDATE ON Artysci
	FOR EACH ROW
	EXECUTE PROCEDURE normArtysci();

CREATE TRIGGER eksponatyTrigger
	BEFORE INSERT OR UPDATE ON Eksponaty
	FOR EACH ROW
	EXECUTE PROCEDURE normEksponaty();

CREATE TRIGGER instytucjeTrigger
	BEFORE INSERT OR UPDATE ON Instytucje
	FOR EACH ROW
	EXECUTE PROCEDURE normInstytucje();
	
CREATE TRIGGER galerieTrigger
	BEFORE INSERT OR UPDATE ON Galerie
	FOR EACH ROW
	EXECUTE PROCEDURE normGalerie();
	
CREATE TRIGGER wystObjTrigger
	BEFORE INSERT OR UPDATE ON WystObj
	FOR EACH ROW
	EXECUTE PROCEDURE normWystObj();

CREATE TRIGGER ekspWInstTrigger
	BEFORE INSERT OR UPDATE ON ekspWInst
	FOR EACH ROW
	EXECUTE PROCEDURE poprawnoscWypozyczeniaInstytucjom();
	
CREATE TRIGGER ekspWGalTrigger
	BEFORE INSERT OR UPDATE ON ekspWGal
	FOR EACH ROW
	EXECUTE PROCEDURE poprawnoscWypozyczeniaGaleriom();
	
CREATE TRIGGER ekspNaWystObjTrigger
	BEFORE INSERT OR UPDATE ON ekspNaWystObj
	FOR EACH ROW
	EXECUTE PROCEDURE poprawnoscWypozyczeniaWystawomObj();

CREATE TRIGGER czyscArtysciTrigger
	AFTER UPDATE OR DELETE ON Eksponaty
	FOR EACH ROW
	EXECUTE PROCEDURE czyscArtysci();

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('jan', 'matejko', 1838, 1893);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('pablo', 'picasso', 1881, 1973);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('michał', 'anioł', 1475, 1564);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('paweł', 'pawłowski', 1990, NULL);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('Adam', 'Jeleń', 0, 33);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('Józef', 'Malina', 1880 , 1930);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('Piotr', 'Mnich', 1996 , NULL);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('Grzegorz', 'Myśliciel', 1990 , NULL);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('Tadeusz', 'Krawiec', 1939 , 1989);

INSERT INTO artysci (imie, nazwisko, rok_urodzenia, rok_smierci)
VALUES ('Grażyna', 'Ząbek', 1960 , NULL);


INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Stworzenie Pana', 1, 'obraz', 10, 9, 8, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Matka Boska', 3, 'rzezba', 5, 5, 800, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Magiczny Kubek', 5, 'przedmiot', 1, 2, 3, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Zaczarowany olowek', 2, 'przedmiot', 4, 3, 2, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Kapelusz ciemnosci', 6, 'przedmiot', 0.1, 0.2, 1, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Kraina zła', 9, 'obraz', 10, 10, 1, 'N');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Zielony kwadrat', 6, 'obraz', 11, 10, 1, 'T');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Obszary nędzy', 5, 'obraz', 5, 4, 1, 'N');

INSERT INTO eksponaty (tytul, id_art, typ, wysokosc, szerokosc, waga, mozna_wyp)
VALUES ('Heros', 9, 'rzezba', 10, 3.5, 80, 'N');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 1');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 2');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 3');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 4');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 5');

INSERT INTO galerie (nazwa)
VALUES ('Galeria 6');


INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (1, to_date('10-12-2014', 'DD-MM-YYYY'), to_date('11-12-2014', 'DD-MM-YYYY'), 5, 1);

INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (1, to_date('10-12-2014', 'DD-MM-YYYY'), to_date('11-12-2014', 'DD-MM-YYYY'), 4, 1);

INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (1, to_date('14-12-2014', 'DD-MM-YYYY'), to_date('16-12-2014', 'DD-MM-YYYY'), 1, 1);

INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (2, to_date('14-12-2014', 'DD-MM-YYYY'), to_date('16-12-2014', 'DD-MM-YYYY'), 3, 2);

INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (2, to_date('17-12-2014', 'DD-MM-YYYY'), to_date('20-12-2014', 'DD-MM-YYYY'), 5, 3);

INSERT INTO ekspwgal (id_gal, data_od, data_do, id_eksp, sala)
VALUES (2, to_date('01-02-2017', 'DD-MM-YYYY'), to_date('20-02-2017', 'DD-MM-YYYY'), 5, 3);

INSERT INTO wystobj (miasto, data_od, data_do)
VALUES ('warszawa', to_date('10-12-2014', 'DD-MM-YYYY'), to_date('15-12-2014', 'DD-MM-YYYY'));

INSERT INTO wystobj (miasto, data_od, data_do)
VALUES ('katowice', to_date('10-05-2015', 'DD-MM-YYYY'), to_date('15-05-2015', 'DD-MM-YYYY'));

INSERT INTO wystobj (miasto, data_od, data_do)
VALUES ('przyłubie', to_date('17-12-2014', 'DD-MM-YYYY'), to_date('20-12-2014', 'DD-MM-YYYY'));

INSERT INTO wystobj (miasto, data_od, data_do)
VALUES ('poznań', to_date('01-01-2015', 'DD-MM-YYYY'), to_date('09-01-2015', 'DD-MM-YYYY'));

INSERT INTO wystobj (miasto, data_od, data_do)
VALUES ('gdańsk', to_date('01-02-2016', 'DD-MM-YYYY'), to_date('09-02-2016', 'DD-MM-YYYY'));

INSERT INTO ekspnawystobj(id_wyst, id_eksp)
VALUES (1, 2);

INSERT INTO ekspnawystobj(id_wyst, id_eksp)
VALUES (1, 8);

INSERT INTO ekspnawystobj(id_wyst, id_eksp)
VALUES (4, 1);


INSERT INTO instytucje(nazwa, miasto)
VALUES ('Intytucja Zakonnic', 'Rzeszów');

INSERT INTO instytucje(nazwa, miasto)
VALUES ('Intytucja Mieszka', 'Radom');

INSERT INTO instytucje(nazwa, miasto)
VALUES ('Galeria Obcych', 'Berlin');

INSERT INTO instytucje(nazwa, miasto)
VALUES ('Galeria Obcych', 'Berlin');

INSERT INTO instytucje(nazwa, miasto)
VALUES ('Kolorowe Kredki', 'Gdańsk');

INSERT INTO ekspwinst(id_inst, data_od, data_do, id_eksp)
VALUES (2, to_date('05-08-2016', 'DD-MM-YYYY'), to_date('09-08-2016', 'DD-MM-YYYY'), 5);

INSERT INTO ekspwinst(id_inst, data_od, data_do, id_eksp)
VALUES (2, to_date('05-08-2017', 'DD-MM-YYYY'), to_date('09-08-2017', 'DD-MM-YYYY'), 3);

INSERT INTO ekspwinst(id_inst, data_od, data_do, id_eksp)
VALUES (3, to_date('01-02-2017', 'DD-MM-YYYY'), to_date('03-02-2017', 'DD-MM-YYYY'), 9);

INSERT INTO uzytkownicy(username, password) 
VALUES ('bolek', 'lolek');



	