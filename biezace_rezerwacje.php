<html>
<head>
<meta charset="utf-8"/>
<title>Bieżące rezerwacje</title>
</head>
<link rel="stylesheet" type="text/css" href="css/table.css">
<body>

<?php
	session_start();
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>

<form action="biezace_rezerwacje.php" method="post">
<?php
	$valid_input = true;
	$nazwa = $tytul = $imie = $nazwisko = '';
	$nazwa_err = $tytul_err = $imie_err = $nazwisko_err = '';
	$query = "";
	if($_SERVER["REQUEST_METHOD"] == "POST") {
	
		//tytul
		if (!empty($_POST[$fn_tytul])) {
			$tytul = test_input($_POST[$fn_tytul]);
			
			if (!preg_match("/^[a-zA-Z0-9 ]*$/",$tytul)) {
				$tytul_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//imie
		if (!empty($_POST[$fn_imie])) {
			$imie = test_input($_POST[$fn_imie]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$imie)) {
				$imie_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//nazwisko
		if (!empty($_POST[$fn_nazwisko])) {
			$nazwisko = test_input($_POST[$fn_nazwisko]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$nazwisko)) {
				$nazwisko_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
	}

	echo "" .
		"Tytuł: <br><input type=text name='$fn_tytul' value='$tytul'>$tytul_err<br>" .
		"Imię: <br><input type=text name='$fn_imie' value='$imie'>$imie_err<br>" .
		"Nazwisko: <br><input type=text name='$fn_nazwisko' value='$nazwisko'>$nazwisko_err<br>" .
		"<input type=submit value='Szukaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" || true) {
	
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		
		$query = "SELECT g.$fn_nazwa, e.$fn_tytul, a.$fn_imie || ' ' || a.$fn_nazwisko as autor, eg.$fn_data_od, eg.$fn_data_do FROM EkspWGal eg " .
			" JOIN Galerie g ON eg.$fn_id_gal = g.$fn_id_gal ".
			" JOIN Eksponaty e ON eg.$fn_id_eksp = e.$fn_id_eksp" .
			" JOIN Artysci a ON e.$fn_id_art = a.$fn_id_art" .
			" WHERE 1=1 AND current_date <= $fn_data_do";

		add_cond_like_to_query($fn_tytul, $tytul);
		add_cond_like_to_query($fn_imie, $imie);
		add_cond_like_to_query($fn_nazwisko, $nazwisko);
		$query = $query . " ORDER BY $fn_data_od";
		$result = pg_exec($link, $query);	
		
		if (!$result) {
			echo pg_last_error($link);
		}
		
		printEkspWGal($result);
		
		//historia na objazdach	
		$query = "SELECT w.$fn_id_wyst, w.$fn_miasto , e.$fn_tytul, a.$fn_imie || ' ' || a.$fn_nazwisko as autor, w.$fn_data_od, w.$fn_data_do FROM ekspNaWystObj eg " .
			" JOIN WystObj w ON eg.$fn_id_wyst = w.$fn_id_wyst ".
			" JOIN Eksponaty e ON eg.$fn_id_eksp = e.$fn_id_eksp" .
			" JOIN Artysci a ON e.$fn_id_art = a.$fn_id_art" .
			" WHERE 1=1 AND current_date <= $fn_data_do";
		
		add_cond_like_to_query($fn_tytul, $tytul);
		add_cond_like_to_query($fn_imie, $imie);
		add_cond_like_to_query($fn_nazwisko, $nazwisko);
		$query = $query . " ORDER BY $fn_data_od";
		$result = pg_exec($link, $query);
		if (!$result) {
			echo pg_last_error($link);
		}
		printEkspNaWystObj($result);
		
		//historia w instytucjach		
		$query = "SELECT i.$fn_id_inst, i.$fn_nazwa, e.$fn_tytul, a.$fn_imie || ' ' || a.$fn_nazwisko as autor, eg.$fn_data_od, eg.$fn_data_do FROM ekspWInst eg " .
			" JOIN Instytucje i ON eg.$fn_id_inst = i.$fn_id_inst ".
			" JOIN Eksponaty e ON eg.$fn_id_eksp = e.$fn_id_eksp" .
			" JOIN Artysci a ON e.$fn_id_art = a.$fn_id_art" .
			" WHERE 1=1 AND current_date <= $fn_data_do";
				
		add_cond_like_to_query($fn_tytul, $tytul);
		add_cond_like_to_query($fn_imie, $imie);
		add_cond_like_to_query($fn_nazwisko, $nazwisko);
		$query = $query . " ORDER BY $fn_data_od";
		$result = pg_exec($link, $query);
		if (!$result) {
			echo pg_last_error($link);
		}
		
		printEkspWInst($result);
			
		pg_close($link);
	}
?>

<?php
	function printEkspWGal($result) {
		global $fn_id_gal;
		global $fn_nazwa;
		global $fn_tytul;
		global $fn_data_od;
		global $fn_data_do;
		
		echo ''.
		'<h2 align=center>rezerwacje w galeriach</h2>

		<table border="1" align=center>
		<tr>
		<th>Galeria</th>
		<th>Tytuł</th>
		<th>Autor</th>
		<th>Data od</th>
		<th>Data do</th>
		</tr>';

		$numrows = pg_numrows($result);
		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo "<td>" . $row[$fn_nazwa] . "</td> 
				<td>" . $row[$fn_tytul] . "</td>
				<td>" . $row['autor'] . "</td>
				<td>" . $row[$fn_data_od] . "</td>
				<td>" . $row[$fn_data_do] . "</td>
				</tr>";
		}
		
		echo '</table>';
	}
	
	function printEkspNaWystObj($result) {
		global $fn_miasto;
		global $fn_tytul;
		global $fn_data_od;
		global $fn_data_do;

		echo '<br>'.
		'<h2 align=center>rezerwacje na wystawach objazdowych</h2>

		<table border="1" align=center>
		<tr>
		<th>Miasto</th>
		<th>Tytuł</th>
		<th>Autor</th>
		<th>Data od</th>
		<th>Data do</th>
		</tr>';

		$numrows = pg_numrows($result);
		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo "<td>" . $row[$fn_miasto] . "</td> 
				<td>" . $row[$fn_tytul] . "</td>
				<td>" . $row['autor'] . "</td>
				<td>" . $row[$fn_data_od] . "</td>
				<td>" . $row[$fn_data_do] . "</td>
				</tr>";
		}
		
		echo '</table>';
	}
	
	function printEkspWInst($result) {
		global $fn_nazwa;
		global $fn_tytul;
		global $fn_data_od;
		global $fn_data_do;
		
		echo '<br>'.
		'<h2 align=center>rezerwacje w instytucjach</h2>

		<table border="1" align=center>
		<tr>
		<th>Instytucja</th>
		<th>Tytuł</th>
		<th>Autor</th>
		<th>Data od</th>
		<th>Data do</th>
		</tr>';

		$numrows = pg_numrows($result);
		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo "<td>" . $row[$fn_nazwa] . "</td> 
				<td>" . $row[$fn_tytul] . "</td>
				<td>" . $row['autor'] . "</td>
				<td>" . $row[$fn_data_od] . "</td>
				<td>" . $row[$fn_data_do] . "</td>
				</tr>";
		}
		
		echo '</table>';
	}
	
?>


</body>
</html>
