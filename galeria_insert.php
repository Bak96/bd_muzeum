<html>
<head>
<meta charset="utf-8"/>
<title>Dodaj galerię</title>
</head>
<body>
<?php
	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php'?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>


<form action="galeria_insert.php" method="post">
<?php
	$valid_input = true;
	$nazwa = '';
	$nazwa_err = '';
	$query = "";
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$nazwa = $_POST[$fn_nazwa];
		
		if (empty($_POST[$fn_nazwa])) {
			$nazwa_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$nazwa = test_input($_POST[$fn_nazwa]);
			
			if (!preg_match("/^[a-zA-Z0-9 ]*$/",$nazwa)) {
				$nazwa_err = "*pole powinno się składać tylko z liter i cyfr";
				$valid_input = false;
			}
		}
	}

	echo "<h1 align='center'>Dodaj galerię: </h1>" .
		"Nazwa: <br><input type=text name='$fn_nazwa' value='$nazwa'>$nazwa_err<br>" .
		"<input type=submit value='Dodaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		$query = "INSERT INTO galerie ($fn_nazwa) " .
			" VALUES ('$nazwa');";
	
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}

		$result = pg_exec($link, $query);
		
		if ($result) {
			echo 'Pomyślnie dodano rekord';
		}
		else {
			echo "Nie udało się dodać rekordu<br>";
			echo pg_last_error($link);
		}

		pg_close($link);
	}
?>

</body>
</html>
