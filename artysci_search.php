<html>
<head>
<meta charset="utf-8"/>
<title>Artysci</title>
</head>
<link rel="stylesheet" type="text/css" href="css/table.css">
<body>
<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>

<?php
	session_start();
?>


<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<form action="artysci_search.php" method="post">
<?php
	$valid_input = true;
	$id_art = $imie = $nazwisko = $rok_urodzenia = $rok_smierci = "";
	$id_art_err = $imie_err = $nazwisko_err = $rok_urodzenia_err = $rok_smierci_err = '';
	$query = "";

	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$imie = $_POST[$fn_imie];
		$nazwisko = $_POST[$fn_nazwisko];
		$rok_urodzenia = $_POST[$fn_rok_urodzenia];
		$rok_smierci = $_POST[$fn_rok_smierci];
		
				
		//id_art
		if (!empty($_POST[$fn_id_art])) {
			$id_art = test_input($_POST[$fn_id_art]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_art)) {
				$id_art_err = "*id powinno się składać tylko z cyfr";
				$valid_input = false;
			}
		}
		
		//imie
		if (!empty($_POST[$fn_imie])) {
			$imie = test_input($_POST[$fn_imie]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$imie)) {
				$imie_err = "*imie powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//nazwisko
		if (!empty($_POST[$fn_nazwisko])) {
			$nazwisko = test_input($_POST[$fn_nazwisko]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$nazwisko)) {
				$nazwisko_err = "*nazwisko powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		
		//rok urodzenia
		if (!empty($_POST[$fn_rok_urodzenia])) {
			$rok_urodzenia = test_input($_POST[$fn_rok_urodzenia]);
			
			if (!preg_match("/^-?[0-9]*$/",$rok_urodzenia)) {
				$rok_urodzenia_err = "*pole powinno składać się z cyfr";
				$valid_input = false;
			}
		}
		
		//rok smierci
		if (!empty($_POST[$fn_rok_smierci])){
			$rok_smierci = test_input($_POST[$fn_rok_smierci]);
			
			if (!preg_match("/^-?[0-9]*$/",$rok_smierci)) {
				$rok_smierci_err = "*pole powinno składać się z cyfr";
				$valid_input = false;
			}
		}
	}

	echo "<h1 align='center'>Wyszukaj artystów: </h1>" .
		"Id: <br><input type=text name='$fn_id_art' value='$id_art'>$id_art_err<br>" .
		"Imię: <br><input type=text name='$fn_imie' value='$imie'>$imie_err<br>" .
		"Nazwisko: <br><input type=text name='$fn_nazwisko' value='$nazwisko'>$nazwisko_err<br>" .
		"Rok urodzenia: <br><input type=text name='$fn_rok_urodzenia' value='$rok_urodzenia'>$rok_urodzenia_err<br>" .
		"Rok śmierci: <br><input type=text name='$fn_rok_smierci' value='$rok_smierci'>$rok_smierci_err<br>".
		"<input type=submit value='Szukaj'>"
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		$query = "SELECT $fn_id_art, $fn_imie, $fn_nazwisko, $fn_rok_urodzenia, $fn_rok_smierci FROM artysci WHERE 1=1";

		add_cond_equal_to_query($fn_id_art, $id_art);
		add_cond_like_to_query($fn_imie, $imie);
		add_cond_like_to_query($fn_nazwisko, $nazwisko);
		add_cond_equal_to_query($fn_rok_urodzenia, $rok_urodzenia);
		add_cond_equal_to_query($fn_rok_smierci, $rok_smierci);
		$query = $query . " ORDER BY $fn_nazwisko";
		
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		
		$result = pg_exec($link, $query);
		$numrows = pg_numrows($result);

		if (!$result) {
			echo pg_last_error($link);
		}
		
		echo '
		<h2 align=center>Artysci</h2>
		<table border="1" align=center>
		<tr>
		<th>Id</th>
		<th>Imię</th>
		<th>Nazwisko</th>
		<th>Rok urodzenia</th>
		<th>Rok śmierci</th>
		</tr>';

		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo " <td>" . $row[$fn_id_art] . "</td>
				<td>" . $row[$fn_imie] . "</td>
				<td>" . $row[$fn_nazwisko] . "</td>
				<td>" . $row[$fn_rok_urodzenia] . "</td>
				<td>" . $row[$fn_rok_smierci] . "</td>
				</tr>";
		}
		pg_close($link);
}
?>




</body>
</html>
