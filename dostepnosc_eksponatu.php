<html>
<head>
<meta charset="utf-8"/>
<title>Dostępnośc eksponatu</title>
</head>
<link rel="stylesheet" type="text/css" href="css/table.css">
<body>
<?php
	session_start();
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>

<form action="dostepnosc_eksponatu.php" method="post">
<?php
	$valid_input = true;
	$id_eksp = $rok = '';
	$id_eksp_err = $rok_err = '';
	$fn_rok = 'rok';
	$query = "";
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {
	
		//id_eksp
		if (empty($_POST[$fn_id_eksp])) {
			$id_eksp_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
		
			$id_eksp = test_input($_POST[$fn_id_eksp]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_eksp)) {
				$id_eksp_err = "*pole powinno się składać tylko z cyfr";
				$valid_input = false;
			}
		}
		
		//rok 
		if (empty($_POST[$fn_rok])) {
			$rok_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$rok = test_input($_POST[$fn_rok]);
			
			if (!preg_match("/^-?[0-9]*$/",$rok)) {
				$rok_err = "*pole powinno składać się z cyfr";
				$valid_input = false;
			}
		}
	}

	echo "<h1 align='center'>Dostępność eksponatu: </h1>" .
		"Id eksponatu:<br><input type=text name='$fn_id_eksp' value='$id_eksp'>$id_eksp_err<br>" .
		"Rok:<br><input type=text name='$fn_rok' value='$rok'>$rok_err<br>" .
		"<input type=submit value='Szukaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
	

		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		
		$query = "SELECT dniPozaMuzeum($id_eksp, $rok) as dni;";
		$result = pg_exec($link, $query);
		
		if (!$result) {
			echo pg_last_error($link);
		}
		
		$row = pg_fetch_array($result, 0);
		echo "<h2 align=center> Maksymalna ilość dni na którą można wypożyczyć eksponat w podanym roku: " . (30-$row['dni']) . " </h2>";
		
		$query = "SELECT $fn_data_od, $fn_data_do FROM ekspWGal " . 
			" WHERE $fn_id_eksp = $id_eksp AND date_part('year',$fn_data_od) = $rok".
			"UNION SELECT $fn_data_od, $fn_data_do FROM ekspWInst " . 
			" WHERE $fn_id_eksp = $id_eksp AND date_part('year',$fn_data_od) = $rok".
			"UNION SELECT $fn_data_od, $fn_data_do FROM ekspNaWystObj e ". 
			"JOIN wystObj w ON e.$fn_id_wyst = w.$fn_id_wyst ".
			" WHERE $fn_id_eksp = $id_eksp AND date_part('year',$fn_data_od) = $rok ".
			" ORDER BY data_od";
		
		$result = pg_exec($link, $query);
		
		if (!$result) {
			echo pg_last_error($link);
		}
		$numrows = pg_numrows($result);
		
		echo ''.
		'<h2 align=center>rezerwacje:</h2>
		
		<table border="1" align=center>
		<tr>
		<th>Data od</th>
		<th>Data do</th>
		</tr>';

		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo "<td>" . $row[$fn_data_od] . "</td> 
				<td>" . $row[$fn_data_do] . "</td>
				</tr>";
		}
		pg_close($link);
	}
?>


</body>
</html>
