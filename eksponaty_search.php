<html>
<head>
<meta charset="utf-8"/>
<title>Eksponaty</title>
</head>
<link rel="stylesheet" type="text/css" href="css/table.css">
<body>
<?php
	session_start();
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>

<form action="eksponaty_search.php" method="post">
<?php
	$valid_input = true;
	$wysokosc_od = $wysokosc_do = $szerokosc_od = $szerokosc_do = $waga_od = $waga_do = $mozna_wyp = '';
	$id_eksp_err = $tytul_err = $id_art_err = $typ_err = $wysokosc_err = $szerokosc_err = $waga_err = $mozna_wyp_err = '';
	$query = "";
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {

		//id_eksp
		if (!empty($_POST[$fn_id_eksp])) {
			$id_eksp = test_input($_POST[$fn_id_eksp]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_eksp)) {
				$id_eksp_err = "*pole się składać tylko z cyfr";
				$valid_input = false;
			}
		}

		//tytul
		if (!empty($_POST[$fn_tytul])) {
			$tytul = test_input($_POST[$fn_tytul]);
			
			if (!preg_match("/^[a-zA-Z0-9 ]*$/",$tytul)) {
				$tytul_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//id_art
		if (!empty($_POST[$fn_id_art])) {
			$id_art = test_input($_POST[$fn_id_art]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_art)) {
				$id_art_err = "*pole powinno się składać tylko z cyfr";
				$valid_input = false;
			}
		}

		//typ
		if (!empty($_POST[$fn_typ])) {
			$typ = test_input($_POST[$fn_typ]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$typ)) {
				$typ_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//wysokosc
		if (!empty($_POST[$fn_wysokosc_od])) {
			$wysokosc_od = test_input($_POST[$fn_wysokosc_od]);
			
			if (!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/",$wysokosc_od)) {
				$wysokosc_err = "*nieprawidłowy format, przykład poprawnego formatu:(3.14)";
				$valid_input = false;
			}
		}
		
		//wysokosc
		if (!empty($_POST[$fn_wysokosc_do])) {
			$wysokosc_do = test_input($_POST[$fn_wysokosc_do]);
			
			if (!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/",$wysokosc_do)) {
				$wysokosc_err = "*nieprawidłowy format, przykład poprawnego formatu:(3.14)";
				$valid_input = false;
			}
		}
		
		//szerokosc od
		if (!empty($_POST[$fn_szerokosc_od])) {
			$szerokosc_od = test_input($_POST[$fn_szerokosc_od]);
			
			if (!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/",$szerokosc_od)) {
				$szerokosc_err = "*nieprawidłowy format, przykład poprawnego formatu:(3.14)";
				$valid_input = false;
			}
		}
		
		//szerokosc do
		if (!empty($_POST[$fn_szerokosc_do])) {
			$szerokosc_do = test_input($_POST[$fn_szerokosc_do]);
			
			if (!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/",$szerokosc_do)) {
				$szerokosc_err = "*nieprawidłowy format, przykład poprawnego formatu:(3.14)";
				$valid_input = false;
			}
		}
		
		//waga od
		if (!empty($_POST[$fn_waga_od])) {
			$waga_od = test_input($_POST[$fn_waga_od]);
			
			if (!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/",$waga_od)) {
				$waga_err = "*nieprawidłowy format, przykład poprawnego formatu:(3.14)";
				$valid_input = false;
			}
		}
		
		//waga do
		if (!empty($_POST[$fn_waga_do])) {
			$waga_do = test_input($_POST[$fn_waga_do]);
			
			if (!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/",$waga_do)) {
				$waga_err = "*nieprawidłowy format, przykład poprawnego formatu:(3.14)";
				$valid_input = false;
			}
		}
		
		//wypozyczalny
		if (!empty($_POST[$fn_mozna_wyp])) {
			$mozna_wyp = test_input($_POST[$fn_mozna_wyp]);
		}
	}

	echo "<h1 align='center'>Wyszukaj eksponaty: </h1>" .
		"Id eksponatu:<br><input type=text name='$fn_id_eksp' value='$id_eksp'>$id_eksp_err<br>" .
		"Tytuł: <br><input type=text name='$fn_tytul' value='$tytul'>$tytul_err<br>" .
		"Id artysty: <br><input type=text name='$fn_id_art' value='$id_art'>$id_art_err<br>" .
		"Typ: <br><input type=text name='$fn_typ' value='$typ'>$typ_err<br>" .
		"Wysokość: <br>od:&nbsp;<input type=text name='$fn_wysokosc_od' value='$wysokosc_od'>".
		"&nbsp;do:&nbsp;<input type=text name='$fn_wysokosc_do' value='$wysokosc_do'>$wysokosc_err<br>".
		"Szerokość: <br>od:&nbsp;<input type=text name='$fn_szerokosc_od' value='$szerokosc_od'>".
		"&nbsp;do:&nbsp;<input type=text name='$fn_szerokosc_do' value='$szerokosc_do'>$szerokosc_err<br>".
		"Waga: <br>od:&nbsp;<input type=text name='$fn_waga_od' value='$waga_od'>".
		"&nbsp;do:&nbsp;<input type=text name='$fn_waga_do' value='$waga_do'>$waga_err<br>".
		"Można wypożyczać: <input type=radio name='$fn_mozna_wyp' ";
	
	if ((isset($mozna_wyp) && $mozna_wyp == "T")) {
		echo "checked ";
	}
	
	echo "value = 'T'> tak" .
		" <input type=radio name='$fn_mozna_wyp' ";
	
	if ((isset($mozna_wyp) && $mozna_wyp == "N")) {
		echo "checked ";
	}
	
	echo "value = 'N'> nie" .
		" <input type=radio name='$fn_mozna_wyp' ";
	
	if ((isset($mozna_wyp) && $mozna_wyp == "")) {
		echo "checked ";
	}
	
	
	
	echo "value = ''> dowolnie $mozna_wyp_err <br>" .
		"<input type=submit value='Szukaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		$query = "SELECT $fn_id_eksp, $fn_tytul, $fn_id_art, $fn_typ, $fn_wysokosc, $fn_szerokosc, $fn_waga, $fn_mozna_wyp " .
			" FROM eksponaty WHERE 1=1";
		
		add_cond_equal_to_query($fn_id_eksp, $id_eksp);
		add_cond_like_to_query($fn_tytul, $tytul);
		add_cond_equal_to_query($fn_id_art, $id_art);
		add_cond_like_to_query($fn_typ, $typ);
		add_cond_greater_eq_to_query($fn_wysokosc, $wysokosc_od);
		add_cond_less_eq_to_query($fn_wysokosc, $wysokosc_do);
		add_cond_greater_eq_to_query($fn_szerokosc, $szerokosc_od);
		add_cond_less_eq_to_query($fn_szerokosc, $szerokosc_do);
		add_cond_greater_eq_to_query($fn_waga, $waga_od);
		add_cond_less_eq_to_query($fn_waga, $waga_do);
		add_cond_equal_to_query($fn_mozna_wyp, $mozna_wyp);
		$query = $query . " ORDER BY $fn_tytul";
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		$result = pg_exec($link, $query);
		if (!$result) {
			echo pg_last_error($link);
		}
		
		$numrows = pg_numrows($result);
		
		echo ''.
		'<h2 align=center>Eksponaty</h2>

		<table border="1" align=center>
		<tr>
		<th>Id eksp</th>
		<th>Tytuł</th>
		<th>Id art</th>
		<th>Typ</th>
		<th>Wysokość</th>
		<th>Szerokość</th>
		<th>Waga</th>
		<th>Można wypożyczać</th>
		</tr>';

		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo "<td>" . $row[$fn_id_eksp] . "</td> 
				<td>" . $row[$fn_tytul] . "</td>
				<td>" . $row[$fn_id_art] . "</td>
				<td>" . $row[$fn_typ] . "</td>
				<td>" . $row[$fn_wysokosc] . "</td>
				<td>" . $row[$fn_szerokosc] . "</td>
				<td>" . $row[$fn_waga] . "</td>
				<td>" . $row[$fn_mozna_wyp] . "</td>
				</tr>";
		}
		pg_close($link);
	}
?>


</body>
</html>
