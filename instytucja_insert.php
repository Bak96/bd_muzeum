<html>
<head>
<meta charset="utf-8"/>
<title>Dodaj instytucję</title>
</head>
<body>
<?php
	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>


<form action="instytucja_insert.php" method="post">
<?php

	$valid_input = true;
	$nazwa = $miasto = '';
	$nazwa_err = $miasto_err = '';
	$query = '';
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		if (empty($_POST[$fn_nazwa])) {
			$nazwa_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$nazwa = test_input($_POST[$fn_nazwa]);
			
			if (!preg_match("/^[a-zA-Z0-9 ]*$/",$nazwa)) {
				$nazwa_err = "*pole powinno się składać tylko z liter i cyfr";
				$valid_input = false;
			}
		}

		if (empty($_POST[$fn_miasto])) {
			$miasto_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$miasto = test_input($_POST[$fn_miasto]);
			
			if (!preg_match("/^[a-zA-Z ]*$/", $miasto)) {
				$miasto_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
	}

	echo "<h1 align='center'>Dodaj instytucję: </h1>" .
		"Nazwa: <br><input type=text name='$fn_nazwa' value='$nazwa'>$nazwa_err<br>" .
		"Miasto: <br><input type=text name='$fn_miasto' value='$miasto'>$miasto_err<br>" .
		"<input type=submit value='Dodaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		$query = "INSERT INTO instytucje($fn_nazwa, $fn_miasto )" .
			" VALUES('$nazwa', '$miasto')";
			
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}

		
		$result = pg_exec($link, $query);
		
		if ($result) {
			echo 'Pomyślnie dodano rekord';
		}
		else {
			echo "Nie udało się dodać rekordu<br>";
			echo pg_last_error($link);
		}
		pg_close($link);
	}
?>

</body>
</html>
