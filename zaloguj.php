<?php include 'helpers.php';?>
<?php
	session_start();
	
	if ((!isset($_POST['login'])) || (!isset($_POST['haslo'])))
	{
		header('Location: logowanie.php');
		exit();
	}

	require_once "connect.php";

	$polaczenie = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
	
	if (!$polaczenie){
		echo "Error: nie udalo sie połączyć z bazą danych!";
	}
	else
	{
		$login = $_POST['login'];
		$haslo = $_POST['haslo'];
		
		$login = test_input($login);
		$haslo = test_input($haslo);
	
		$zapytanie = "SELECT * FROM uzytkownicy WHERE username='$login' AND password='$haslo'";
	
		if ($rezultat = @pg_exec($zapytanie)) {
			$ilu_userow = pg_numrows($rezultat);
			
			if($ilu_userow >0) {
				$_SESSION['zalogowany'] = true;
				
				$wiersz = pg_fetch_array($rezultat, 0);
				
				$_SESSION['username'] = $wiersz['username'];
				
				unset($_SESSION['blad']);
				header('Location: index.php');
				
			} else {
				
				$_SESSION['blad'] = '<span style="color:red">Nieprawidłowy login lub hasło!</span>';
				header('Location: logowanie.php');
				
			}
			
		}
		
		pg_close($polaczenie);
	}
	
?>