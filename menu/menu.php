<?php
echo '
<link rel="stylesheet" type="text/css" href="css/menu.css">

<ul>
  <li><a href="index.php">Home</a></li>
  <li class="dropdown">
    <a href="javascript:void(0)" class="dropbtn">Wyszukaj</a>
    <div class="dropdown-content">
      <a href="artysci_search.php">Artystów</a>
      <a href="eksponaty_search.php">Eksponaty</a>
	  <a href="galerie_search.php">Galerie</a>
      <a href="instytucje_search.php">Instytucje</a>
      <a href="wystawy_obj_search.php">Wystawy Objazdowe</a>
    </div>
  </li>
  
  <li class="dropdown">
    <a href="javascript:void(0)" class="dropbtn">Historia</a>
    <div class="dropdown-content">
      <a href="eksponat_history.php">Eksponatu</a>
      <a href="galeria_history.php">Galerii</a>
      <a href="instytucja_history.php">Instytucji</a>
      <a href="wystawa_obj_history.php">Wystawy objazdowej</a>
    </div>
  </li>
  

  <li class="dropdown">
    <a href="javascript:void(0)" class="dropbtn">Sprawdź</a>
    <div class="dropdown-content">
      <a href="biezace_wystawy.php">Bieżące wystawy</a>
      <a href="biezace_rezerwacje.php">Bieżące rezerwacje</a>
      <a href="dostepnosc_eksponatu.php">Dostępność eksponatu</a>
    </div>
  </li>';
  if (isset($_SESSION['zalogowany'])) {
	echo '<li class="dropdown">
			<a href="javascript:void(0)" class="dropbtn">Dodaj</a>
			<div class="dropdown-content">
			<a href="artysta_insert.php">Artystę</a>
			<a href="eksponat_insert.php">Eksponat</a>
			<a href="galeria_insert.php">Galerię</a>
			<a href="instytucja_insert.php">Instytucję</a>
			<a href="wystawa_obj_insert.php">Wystawę Objadową </a>
			</div>
		</li>
		<li class="dropdown">
			<a href="javascript:void(0)" class="dropbtn">Przenieś/Wypożycz</a>
			<div class="dropdown-content">
			<a href="galeria_lend.php">Do galerii</h>
			<a href="wystawa_obj_lend.php">Na wystawę objazdową</a>
			<a href="instytucja_lend.php">Innej instytucji</a>
			</div>
		</li>
		<li style="float:right"><a href="logout.php">Wyloguj</a></li>';
  }
  else {
	echo '<li style="float:right"><a href="logowanie.php">Zaloguj</a></li>';
  
  }
  
echo '
</ul>
';
?>