<html>
<head>
<meta charset="utf-8"/>
<title>Dodaj Artystę</title>
</head>

<body>

<?php
	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>
<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>


<form action="artysta_insert.php" method="post">
<?php
	$valid_input = true;
	$imie = $nazwisko = $rok_urodzenia = $rok_smierci = $id_eksp = "";
	$imie_err = $nazwisko_err = $rok_urodzenia_err = $rok_smierci_err = $id_eksp_err = '';
	$query = "";

	if($_SERVER["REQUEST_METHOD"] == "POST") {
		//imie
		if (empty($_POST[$fn_imie])) {
			$imie_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$imie = test_input($_POST[$fn_imie]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$imie)) {
				$imie_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//nazwisko
		if (empty($_POST[$fn_nazwisko])) {
			$nazwisko_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$nazwisko = test_input($_POST[$fn_nazwisko]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$nazwisko)) {
				$nazwisko_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//rok urodzenia
		if (empty($_POST[$fn_rok_urodzenia])) {
			$rok_urodzenia_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$rok_urodzenia = test_input($_POST[$fn_rok_urodzenia]);
			
			if (!preg_match("/^-?[0-9]*$/",$rok_urodzenia)) {
				$rok_urodzenia_err = "*pole powinno składać się z cyfr";
				$valid_input = false;
			}
		}
		
		//rok smierci
		if (!empty($_POST[$fn_rok_smierci])){
			$rok_smierci = test_input($_POST[$fn_rok_smierci]);
			
			if (!preg_match("/^-?[0-9]*$/",$rok_smierci)) {
				$rok_urodzenia_err = "*pole powinno składać się z cyfr";
				$valid_input = false;
			}
			else if ($valid_input && $rok_smierci < $rok_urodzenia) {
				$rok_smierci_err = "*rok śmierci powinien byc wiekszy od roku urodzenia";
			}
		}
		
		//id_eksp
		if (empty($_POST[$fn_id_eksp])) {
			$id_eksp_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$id_eksp = test_input($_POST[$fn_id_eksp]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_eksp)) {
				$id_eksp_err = "*pole się składać tylko z cyfr";
				$valid_input = false;
			}
		}
	}

	echo "<h1 align='center'>Dodaj artystę: </h1>" .
		"Imię:<br> <input type=text name='$fn_imie' value='$imie'>$imie_err <br>" .
		"Nazwisko:<br> <input type=text name='$fn_nazwisko' value='$nazwisko'>$nazwisko_err <br>" .
		"Rok urodzenia: <br><input type=text name='$fn_rok_urodzenia' value='$rok_urodzenia'>$rok_urodzenia_err <br>" .
		"Rok śmierci: <br><input type=text name='$fn_rok_smierci' value='$rok_smierci'>$rok_smierci_err <br>".
		"Id eksponatu:<br><input type=text name='$fn_id_eksp' value='$id_eksp'>$id_eksp_err<br>" .
		"<input type=submit value='Dodaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		$query = "INSERT INTO artysci ($fn_imie, $fn_nazwisko, $fn_rok_urodzenia, $fn_rok_smierci) " .
			" VALUES('$imie', '$nazwisko', $rok_urodzenia, ";
		if (empty($rok_smierci)) {
			$query = $query . "NULL";
		}
		$query = $query . "$rok_smierci) RETURNING $fn_id_art;";
		
		
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}

		$result = pg_exec($link, $query);
		if ($result) {
			
			$row = pg_fetch_array($result, 0);
		//	echo "<h2 align=center> NOWE ID: " . ($row["$fn_id_art"]) . " </h2>";
			$id_art = $row["$fn_id_art"];
			$query_update = "UPDATE eksponaty SET $fn_id_art = $id_art WHERE $fn_id_eksp = $id_eksp;";
			$result2 = pg_exec($link, $query_update);
			
			if (!$result2) {
				echo pg_last_error($link);
			}
			else {
				echo 'Pomyślnie dodano rekord';
			}
		}
		else {
			echo pg_last_error($link);
		}
		pg_close($link);
	}
?>

</body>
</html>
