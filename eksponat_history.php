<html>
<head>
<meta charset="utf-8"/>
<title>Historia eksponatu</title>
</head>
<link rel="stylesheet" type="text/css" href="css/table.css">
<body>
<?php
	session_start();
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>

<form action="eksponat_history.php" method="post">
<?php
	
	$id_eksp = $data_od = $data_do = '';
	$id_eksp_err = $data_od_err = $data_do_err = '';
	$valid_input = true;
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		//id_eksp
		if (empty($_POST[$fn_id_eksp])) {
			$id_eksp_err = '*pole wymagane';
			$valid_input = false;
		}
		else {
			$id_eksp = test_input($_POST[$fn_id_eksp]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_eksp)) {
				$id_eksp_err = "*pole powinno się składać tylko z cyfr";
				$valid_input = false;
			}
		}

		//data_od
		if (!empty($_POST[$fn_data_od])) {
			$data_od = test_input($_POST[$fn_data_od]);
			
			if (!validate_date($data_od)) {
				$data_od_err = "*wprowadzono nieprawidłowy format";
				$valid_input = false;
			}
		}
		
		//data_do
		if (!empty($_POST[$fn_data_do])) {
			$data_do = test_input($_POST[$fn_data_do]);
			
			if (!validate_date($data_do)) {
				$data_do_err = "*wprowadzono nieprawidłowy format";
				$valid_input = false;
			}
		}
	}

	echo "<h1 align='center'>Historia eksponatu: </h1>" .
		"Id eksponatu: <br><input type=text name='$fn_id_eksp' value='$id_eksp'>$id_eksp_err<br>" .
		"Data od: (DD-MM-YYYY)<br> <input type=text name='$fn_data_od' value='$data_od'>$data_od_err<br>" .
		"Data do: (DD-MM-YYYY)<br> <input type=text name='$fn_data_do' value='$data_do'>$data_do_err<br>" .
		"<input type=submit value='Szukaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		//historia w galeriach
		$query = "SELECT e.$fn_id_gal, g.$fn_nazwa, e.$fn_data_od, e.$fn_data_do " .
			" FROM ekspwgal e JOIN galerie g ON e.$fn_id_gal = g.$fn_id_gal " .
			" WHERE 1=1 ";
		add_cond_equal_to_query($fn_id_eksp, $id_eksp);
		add_cond_date_less_eq_to_query($fn_data_od, $data_do);
		add_cond_date_greater_eq_to_query($fn_data_do, $data_od);
		$query = $query . " ORDER BY $fn_data_od";
		
		$result = pg_exec($link, $query);	
		if (!$result) {
			echo pg_last_error($link);
		}
		printEkspWGal($result);
		
		//historia na objazdach
		$query = "SELECT e.$fn_id_wyst, w.$fn_miasto, w.$fn_data_od, w.$fn_data_do " .
			" FROM ekspnawystobj e JOIN wystobj w on e.$fn_id_wyst = w.$fn_id_wyst ";
		add_cond_equal_to_query($fn_id_eksp, $id_eksp);
		add_cond_date_less_eq_to_query($fn_data_od, $data_do);
		add_cond_date_greater_eq_to_query($fn_data_do, $data_od);
		$query = $query . " ORDER BY $fn_data_od";
		$result = pg_exec($link, $query);
		if (!$result) {
			echo pg_last_error($link);
		}
		printEkspNaWystObj($result);
		
		//historia w instytucjach
		$query = "SELECT e.$fn_id_inst, i.$fn_nazwa, e.$fn_data_od, e.$fn_data_do " .
			" FROM ekspwinst e JOIN instytucje i ON e.$fn_id_inst = i.$fn_id_inst ";
		add_cond_equal_to_query($fn_id_eksp, $id_eksp);
		add_cond_date_less_eq_to_query($fn_data_od, $data_do);
		add_cond_date_greater_eq_to_query($fn_data_do, $data_od);
		$query = $query . " ORDER BY $fn_data_od";
		$result = pg_exec($link, $query);
		if (!$result) {
			echo pg_last_error($link);
		}
		
		printEkspWInst($result);
			
		pg_close($link);
	}
?>

<?php
	function printEkspWGal($result) {
		global $fn_id_gal;
		global $fn_nazwa;
		global $fn_data_od;
		global $fn_data_do;
		
		echo ''.
		'<h2 align=center>Ekspozycje w Galeriach</h2>

		<table border="1" align=center>
		<tr>
		<th>Id galerii</th>
		<th>Nazwa</th>
		<th>Data od</th>
		<th>Data do</th>
		</tr>';

		$numrows = pg_numrows($result);
		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo "<td>" . $row[$fn_id_gal] . "</td> 
				<td>" . $row[$fn_nazwa] . "</td>
				<td>" . $row[$fn_data_od] . "</td>
				<td>" . $row[$fn_data_do] . "</td>
				</tr>";
		}
		
		echo '</table>';
	}
	
	function printEkspNaWystObj($result) {
		global $fn_id_wyst;
		global $fn_miasto;
		global $fn_data_od;
		global $fn_data_do;

		echo ''.
		'<h2 align=center>Ekspozycje na wystawach objazdowych</h2>

		<table border="1" align=center>
		<tr>
		<th>Id wystawy</th>
		<th>Miasto</th>
		<th>Data od</th>
		<th>Data do</th>
		</tr>';

		$numrows = pg_numrows($result);
		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo "<td>" . $row[$fn_id_wyst] . "</td> 
				<td>" . $row[$fn_miasto] . "</td>
				<td>" . $row[$fn_data_od] . "</td>
				<td>" . $row[$fn_data_do] . "</td>
				</tr>";
		}
		
		echo '</table>';
	}
	
	function printEkspWInst($result) {
		global $fn_id_inst;
		global $fn_nazwa;
		global $fn_data_od;
		global $fn_data_do;
		
		echo ''.
		'<h2 align=center>Ekspozycje w instytucjach</h2>

		<table border="1" align=center>
		<tr>
		<th>Id instytucji</th>
		<th>Nazwa</th>
		<th>Data od</th>
		<th>Data do</th>
		</tr>';

		$numrows = pg_numrows($result);
		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo "<td>" . $row[$fn_id_inst] . "</td> 
				<td>" . $row[$fn_nazwa] . "</td>
				<td>" . $row[$fn_data_od] . "</td>
				<td>" . $row[$fn_data_do] . "</td>
				</tr>";
		}
		
		echo '</table>';
	}
?>


</body>
</html>
