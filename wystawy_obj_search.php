<html>
<head>
<meta charset="utf-8"/>
<title>Wystawy objazdowe</title>
</head>
<link rel="stylesheet" type="text/css" href="css/table.css">
<body>
<?php
	session_start();
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>


<form action="wystawy_obj_search.php" method="post">
<?php
	$valid_input = true;
	$id_wyst = $miasto = $data_od = $data_do = '';
	$id_wyst_err = $miasto_err = $data_od_err = $data_do_err = '';
	$query = "";
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {

		if (!empty($_POST[$fn_id_wyst])) {
			$id_wyst = test_input($_POST[$fn_id_wyst]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_wyst)) {
				$id_wyst_err = "*pole powinno się składać tylko z cyfr";
				$valid_input = false;
			}
		}

		//miasto
		if (!empty($_POST[$fn_miasto])) {
			$miasto = test_input($_POST[$fn_miasto]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$miasto)) {
				$miasto_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//data_od
		if (!empty($_POST[$fn_data_od])) {
			$data_od = test_input($_POST[$fn_data_od]);
			
			if (!validate_date($data_od)) {
				$data_od_err = "*wprowadzono nieprawidłowy format";
				$valid_input = false;
			}
		}
		
		//data_do
		if (!empty($_POST[$fn_data_do])) {
			$data_do = test_input($_POST[$fn_data_do]);
			
			if (!validate_date($data_do)) {
				$data_do_err = "*wprowadzono nieprawidłowy format";
				$valid_input = false;
			}
		}
	}

	echo "<h1 align='center'>Wyszukaj wystaw objazdowych: </h1>" .
		"Id wystawy: <br><input type=text name='$fn_id_wyst' value='$id_wyst'>$id_wyst_err<br>" .
		"Miasto:<br> <input type=text name='$fn_miasto' value='$miasto'>$miasto_err<br>" .
		"Data od:(DD-MM-YYYY)<br> <input type=text name='$fn_data_od' value='$data_od'>$data_od_err<br>" .
		"Data do:(DD-MM-YYYY)<br> <input type=text name='$fn_data_do' value='$data_do'>$data_do_err<br>" .
		"<input type=submit value='Szukaj'>";
?>
</form>

<?php

	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		$query = "SELECT $fn_id_wyst, $fn_miasto, $fn_data_od, $fn_data_do " .
			" FROM wystobj WHERE 1=1";
			
		add_cond_equal_to_query($fn_id_wyst, $id_wyst);
		add_cond_like_to_query($fn_miasto, $miasto);
		add_cond_date_greater_eq_to_query($fn_data_od, $data_od);
		add_cond_date_less_eq_to_query($fn_data_do, $data_do);
		
		$query = $query . " ORDER BY $fn_data_od";
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		$result = pg_exec($link, $query);
		if (!$result) {
			echo pg_last_error($link);
		}
		
		printInst($result);

		pg_close($link);
	}
?>

<?php
	function printInst($result) {
		global $fn_id_wyst, $fn_miasto, $fn_data_od, $fn_data_do;
		
		echo ''.
		'<h2 align=center>Wystawy objazdowe</h2>

		<table border="1" align=center>
		<tr>
		<th>Id wystawy</th>
		<th>Miasto</th>
		<th>Data od</th>
		<th>Data do</th>
		</tr>';

		$numrows = pg_numrows($result);
		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo "<td>" . $row[$fn_id_wyst] . "</td> 
				<td>" . $row[$fn_miasto] . "</td>
				<td>" . $row[$fn_data_od] . "</td>
				<td>" . $row[$fn_data_do] . "</td>
				</tr>";
		}
		
		echo '</table>';
	}
?>



</body>
</html>
