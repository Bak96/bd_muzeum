<html>
<head>
<meta charset="utf-8"/>
<title>Dodaj eksponat</title>
</head>
<body>
<?php
	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>


<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>

<form action="eksponat_insert.php" method="post">
<?php
	$valid_input = true;
	$tytul = $id_art = $typ = $wysokosc = $szerokosc = $waga = $mozna_wyp = '';
	$tytul_err = $id_art_err = $typ_err = $wysokosc_err = $szerokosc_err = $waga_err = $mozna_wyp_err = '';
	$query = "";
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {

		//tytul
		if (empty($_POST[$fn_tytul])) {
			$tytul_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$tytul = test_input($_POST[$fn_tytul]);
			
			if (!preg_match("/^[a-zA-Z0-9 ]*$/",$tytul)) {
				$tytul_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		if (!empty($_POST[$fn_id_art])) {
			$id_art = test_input($_POST[$fn_id_art]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_art)) {
				$id_art_err = "*pole powinno się składać tylko z cyfr";
				$valid_input = false;
			}
		}
		
		//typ
		if (empty($_POST[$fn_typ])) {
			$typ_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$typ = test_input($_POST[$fn_typ]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$typ)) {
				$typ_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//wysokosc
		if (empty($_POST[$fn_wysokosc])) {
			$wysokosc_err = "*pole wysokosc wymagane";
			$valid_input = false;
		}
		else {
			$wysokosc = test_input($_POST[$fn_wysokosc]);
			
			if (!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/",$wysokosc)) {
				$wysokosc_err = "*nieprawidłowy format, przykład poprawnego formatu:(3.14)";
				$valid_input = false;
			}
		}
		
		//szerokosc
		if (empty($_POST[$fn_szerokosc])) {
			$szerokosc_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$szerokosc = test_input($_POST[$fn_szerokosc]);
			
			if (!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/",$szerokosc)) {
				$szerokosc_err = "*nieprawidłowy format, przykład poprawnego formatu:(3.14)";
				$valid_input = false;
			}
		}
		
		//waga
		if (empty($_POST[$fn_waga])) {
			$waga_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$waga = test_input($_POST[$fn_waga]);
			
			if (!preg_match("/^[0-9]+(\.[0-9]{1,2})?$/",$waga)) {
				$waga_err = "*nieprawidłowy format, przykład poprawnego formatu:(3.14)";
				$valid_input = false;
			}
		}
		
		//wypozyczalny
		if (empty($_POST[$fn_mozna_wyp])) {
			$mozna_wyp_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$mozna_wyp = test_input($_POST[$fn_mozna_wyp]);
		}
		
	}

	
	echo "<h1 align='center'>Dodaj eksponat: </h1>" .
		"Tytuł: <br><input type=text name='$fn_tytul' value='$tytul'>$tytul_err<br>" .
		"Id artysty:<br> <input type=text name='$fn_id_art' value='$id_art'>$id_art_err<br>" .
		"Typ:<br> <input type=text name='$fn_typ' value='$typ'>$typ_err<br>" .
		"Wysokość: <br><input type=text name='$fn_wysokosc' value='$wysokosc'>$wysokosc_err<br>".
		"Szerokość:<br> <input type=text name='$fn_szerokosc' value='$szerokosc'>$szerokosc_err<br>".
		"Waga: <br><input type=text name='$fn_waga' value='$waga'>$waga_err<br>".
		"<br>Można wypożyczyć: <input type=radio name='$fn_mozna_wyp' ";
	
	if ((isset($mozna_wyp) && $mozna_wyp == "T")) {
		echo "checked ";
	}
	
	echo "value = 'T'> tak" .
		" <input type=radio name='$fn_mozna_wyp' ";
	
	if ((isset($mozna_wyp) && $mozna_wyp == "N")) {
		echo "checked ";
	}
		
	echo "value = 'N'> nie $mozna_wyp_err<br><br>" .
		"<input type=submit value='Dodaj'>";
?>
</form>


<?php
	function add_condition_to_query($field, $value) {
		global $query;
		if ($value != '') {
			$query = $query . " AND $field = '$value'";
		}
	}
?>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		if (!empty($id_art)) {
			$query = "INSERT INTO eksponaty ($fn_tytul, $fn_id_art, $fn_typ, $fn_wysokosc, $fn_szerokosc, $fn_waga, $fn_mozna_wyp) " .
				" VALUES ('$tytul', '$id_art', '$typ', '$wysokosc', '$szerokosc', '$waga', '$mozna_wyp')";
		}else {
			$query = "INSERT INTO eksponaty ($fn_tytul, $fn_id_art, $fn_typ, $fn_wysokosc, $fn_szerokosc, $fn_waga, $fn_mozna_wyp) " .
				" VALUES ('$tytul', NULL, '$typ', '$wysokosc', '$szerokosc', '$waga', '$mozna_wyp')";
		}
		
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		$result = pg_exec($link, $query);

		if ($result) {
			echo 'Pomyślnie dodano rekord';
		}
		else {
			echo "Nie udało się dodać rekordu<br>";
			echo pg_last_error($link);
		}
		
		pg_close($link);
	}
?>


</body>
</html>
