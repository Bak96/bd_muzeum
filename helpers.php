<?php 
	function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
	
	function validate_date($date, $format = 'd-m-Y')
	{
		$d = DateTime::createFromFormat($format, $date);

		return $d && $d->format($format) == $date;
	}
	
	
	function add_cond_greater_eq_to_query($field, $value) {
		global $query;
		$value = strtoupper($value);
		
		if (!empty($value)) {
			$query = $query . " AND $field >= '$value' ";
		}
	}
	
	function add_cond_less_eq_to_query($field, $value) {
		global $query;
		$value = strtoupper($value);
		
		if (!empty($value)) {
			$query = $query . " AND $field <= '$value' ";
		}
	}
	
	function add_cond_date_less_eq_to_query($field, $value) {
		global $query;
		$value = strtoupper($value);
		
		if (!empty($value)) {
			$query = $query . " AND $field <= to_date('$value', 'DD-MM-YYYY') ";
		}
	}
	
	function add_cond_date_greater_eq_to_query($field, $value) {
		global $query;
		$value = strtoupper($value);
		
		if (!empty($value)) {
			$query = $query . " AND $field >= to_date('$value', 'DD-MM-YYYY') ";
		}
	}
	

	function add_cond_like_to_query($field, $value) {
		global $query;
		$value = strtoupper($value);
		
		if (!empty($value)) {
			$query = $query . " AND $field LIKE '%$value%' ";
		}
	}

	function add_cond_equal_to_query($field, $value) {
		global $query;
		$value = strtoupper($value);
		if (!empty($value)) {
			$query = $query . " AND $field = '$value'";
		}
	}
	
	function valid_date_interval($date1, $date2, $format = 'd-m-Y') {
		$d1 = DateTime::createFromFormat($format, $date1);
		$d2 = DateTime::createFromFormat($format, $date2);
		
		return $d1 <= $d2;
	}
	
?>