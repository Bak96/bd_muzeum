<html>
<head>
<meta charset="utf-8"/>
<title>Historia instytucji</title>
</head>
<link rel="stylesheet" type="text/css" href="css/table.css">
<body>
<?php
	session_start();
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>

<form action="instytucja_history.php" method="post">
<?php
	$valid_input = true;
	$id_inst = $tytul = $imie = $nazwisko = $data_od = $data_do = '';
	$id_inst_err = $tytul_err = $imie_err = $nazwisko_err = $data_od_err = $data_do_err = '';
	$query = "";
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {
	
		//id_gal
		if (empty($_POST[$fn_id_inst])) {
			$id_inst_err = '*pole wymagane';
			$valid_input = false;
		}
		else {
			$id_inst = test_input($_POST[$fn_id_inst]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_inst)) {
				$id_inst_err = "*pole powinno się składać tylko z cyfr";
				$valid_input = false;
			}
		}
	
		//tytul
		if (!empty($_POST[$fn_tytul])) {
			$tytul = test_input($_POST[$fn_tytul]);
			
			if (!preg_match("/^[a-zA-Z0-9 ]*$/",$tytul)) {
				$tytul_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//imie
		if (!empty($_POST[$fn_imie])) {
			$imie = test_input($_POST[$fn_imie]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$imie)) {
				$imie_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//nazwisko
		if (!empty($_POST[$fn_nazwisko])) {
			$nazwisko = test_input($_POST[$fn_nazwisko]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$nazwisko)) {
				$nazwisko_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//data_od
		if (!empty($_POST[$fn_data_od])) {
			$data_od = test_input($_POST[$fn_data_od]);
			
			if (!validate_date($data_od)) {
				$data_od_err = "*wprowadzono nieprawidłowy format";
				$valid_input = false;
			}
		}
		
		//data_do
		if (!empty($_POST[$fn_data_do])) {
			$data_do = test_input($_POST[$fn_data_do]);
			
			if (!validate_date($data_do)) {
				$data_do_err = "*wprowadzono nieprawidłowy format";
				$valid_input = false;
			}
		}
		
	}

	echo "<h1 align='center'>Historia instytucji: </h1>" .
		"Id instytucji:<br><input type=text name='$fn_id_inst' value='$id_inst'>$id_inst_err<br>" .
		"Tytuł: <br><input type=text name='$fn_tytul' value='$tytul'>$tytul_err<br>" .
		"Imię: <br><input type=text name='$fn_imie' value='$imie'>$imie_err<br>" .
		"Nazwisko: <br><input type=text name='$fn_nazwisko' value='$nazwisko'>$nazwisko_err<br>" .
		"Data od:(DD-MM-YYYY)<br> <input type=text name='$fn_data_od' value='$data_od'>$data_od_err<br>" .
		"Data do:(DD-MM-YYYY)<br> <input type=text name='$fn_data_do' value='$data_do'>$data_do_err<br>" .
		"<input type=submit value='Szukaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {

		//historia w instytucjach		
		$query = "SELECT i.$fn_id_inst, i.$fn_nazwa, e.$fn_tytul, a.$fn_imie || ' ' || a.$fn_nazwisko as autor, eg.$fn_data_od, eg.$fn_data_do FROM ekspWInst eg " .
			" JOIN Instytucje i ON eg.$fn_id_inst = i.$fn_id_inst ".
			" JOIN Eksponaty e ON eg.$fn_id_eksp = e.$fn_id_eksp" .
			" JOIN Artysci a ON e.$fn_id_art = a.$fn_id_art" .
			" WHERE 1=1 ";
		
		add_cond_equal_to_query("eg.$fn_id_inst", $id_inst);
		add_cond_like_to_query($fn_tytul, $tytul);
		add_cond_like_to_query($fn_imie, $imie);
		add_cond_like_to_query($fn_nazwisko, $nazwisko);
		add_cond_date_less_eq_to_query($fn_data_od, $data_do);
		add_cond_date_greater_eq_to_query($fn_data_do, $data_od);
		$query = $query . " ORDER BY $fn_data_od";
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		
		$result = pg_exec($link, $query);
		if (!$result) {
			echo pg_last_error($link);
		}
		
		$numrows = pg_numrows($result);
		
		echo ''.
		'<h2 align=center>Historia wystaw w zewnętrznych instytucjach</h2>

		<table border="1" align=center>
		<tr>
		<th>Instytucja</th>
		<th>Tytuł</th>
		<th>Autor</th>
		<th>Data od</th>
		<th>Data do</th>
		</tr>';

		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo "<td>" . $row[$fn_nazwa] . "</td> 
				<td>" . $row[$fn_tytul] . "</td>
				<td>" . $row['autor'] . "</td>
				<td>" . $row[$fn_data_od] . "</td>
				<td>" . $row[$fn_data_do] . "</td>
				</tr>";
		}
		pg_close($link);
	}
?>


</body>
</html>
