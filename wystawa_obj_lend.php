<html>
<head>
<meta charset="utf-8"/>
<title>Wypożycz na wystawę objazdową</title>
</head>
<body>
<?php
	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>


<form action="wystawa_obj_lend.php" method="post">
<?php

	$valid_input = true;
	
	$id_wyst = $id_eksp = '';
	$id_wyst_err = $id_eksp_err = '';
	$query = '';
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		if (empty($_POST[$fn_id_wyst])) {
			$id_wyst_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$id_wyst = test_input($_POST[$fn_id_wyst]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_wyst)) {
				$id_wyst_err = "*pole powinno składać się z samych cyfr";
				$valid_input = false;
			}
		}

		if (empty($_POST[$fn_id_eksp])) {
			$id_eksp_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$id_eksp = test_input($_POST[$fn_id_eksp]);
			
			if (!preg_match("/^[1-9][0-9]*$/", $id_eksp)) {
				$id_eksp_err = "*pole powinno składać się z samych cyfr";
				$valid_input = false;
			}
		}
	}

	echo "<h1 align='center'>Wypożycz na wystawę objazdową: </h1>" .
		"Id wystawy objazdowej: <br><input type=text name='$fn_id_wyst' value='$id_wyst'>$id_wyst_err<br>" .
		"Id eksponatu: <br><input type=text name='$fn_id_eksp' value='$id_eksp'>$id_eksp_err<br>" .
		"<input type=submit value='Dodaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		$query = "INSERT INTO ekspnawystobj($fn_id_wyst, $fn_id_eksp)" .
			" VALUES('$id_wyst', '$id_eksp')";
			
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		$result = pg_exec($link, $query);
		
		if ($result) {
			echo 'Pomyślnie dodano rekord';
		}
		else {
			echo "Nie udało się dodać rekordu";
			echo pg_last_error($link);
		}
		pg_close($link);
	}
?>

</body>
</html>
