<html>
<head>
<meta charset="utf-8"/>
<title>Dodaj wystawę objazdową</title>
</head>
<body>
<?php
	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>


<form action="wystawa_obj_insert.php" method="post">
<?php
	$valid_input = true;
	$miasto = $data_od = $data_do = '';
	$miasto_err = $data_od_err = $data_do_err = '';
	$query = "";
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {	
		//miasto
		if (empty($_POST[$fn_miasto])) {
			$miasto_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$miasto = test_input($_POST[$fn_miasto]);
			
			if (!preg_match("/^[a-zA-Z ]*$/",$miasto)) {
				$miasto_err = "*pole powinno się składać tylko z liter alfabetu";
				$valid_input = false;
			}
		}
		
		//data_od
		if (empty($_POST[$fn_data_od])) {
			$data_od_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$data_od = test_input($_POST[$fn_data_od]);
			
			if (!validate_date($data_od)) {
				$data_od_err = "*wprowadzono nieprawidłowy format";
				$valid_input = false;
			}
		}
		
		//data_do
		if (empty($_POST[$fn_data_do])) {
			$data_do_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$data_do = test_input($_POST[$fn_data_do]);
			
			if (!validate_date($data_do)) {
				$data_do_err = "*wprowadzono nieprawidłowy format";
				$valid_input = false;
			}
		}
		
		if ($valid_input == true && !valid_date_interval($data_od, $data_do)) {
			$data_do_err = "*data do powinna byc pozniejsza niz data od";
			$valid_input = false;
		}
	}

	echo "<h1 align='center'>Dodaj wystawę objazdową: </h1>" .
		"Miasto: <br><input type=text name='$fn_miasto' value='$miasto'>$miasto_err<br>" .
		"Data od:(DD-MM-YYYY)<br><input type=text name='$fn_data_od' value='$data_od'>$data_od_err<br>" .
		"Data do:(DD-MM-YYYY) <br><input type=text name='$fn_data_do' value='$data_do'>$data_do_err<br>" .
		"<input type=submit value='Dodaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		$query = "INSERT INTO wystobj($fn_miasto, $fn_data_od, $fn_data_do)" .
			" VALUES('$miasto', to_date('$data_od','DD-MM-YYYY'), to_date('$data_do','DD-MM-YYYY'))";
		
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		$result = pg_exec($link, $query);
	
		if ($result) {
			echo 'Pomyślnie dodano rekord';
		}
		else {
			echo "nie udało się dodać rekordu";
			echo pg_last_error($link);
		}
	
		pg_close($link);
	}
?>

<?php

	function add_condition_to_query($field, $value) {
		global $query;
		if ($value != '') {
			$query = $query . " AND $field = '$value'";
		}
	}
?>



</body>
</html>
