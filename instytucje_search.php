<html>
<head>
<meta charset="utf-8"/>
<title>Instytucje</title>
</head>
<link rel="stylesheet" type="text/css" href="css/table.css">
<body>
<?php
	session_start();
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>


<form action="instytucje_search.php" method="post">
<?php
	$valid_input = true;
	$id_inst = $nazwa = $miasto = '';
	$id_inst_err = $nazwa_err = $miasto_err = '';
	
	$query = "";
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		$id_inst = $_POST[$fn_id_inst];
		$nazwa = $_POST[$fn_nazwa];
		$miasto = $_POST[$fn_miasto];
		
		if (!empty($_POST[$fn_id_inst])) {
			$id_inst = test_input($_POST[$fn_id_inst]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_inst)) {
				$id_inst_err = "*pole powinno się składać tylko z cyfr";
				$valid_input = false;
			}
		}
		
		//nazwa
		if (!empty($_POST[$fn_nazwa])) {
			$nazwa = test_input($_POST[$fn_nazwa]);
			
			if (!preg_match("/^[a-zA-Z0-9 ]*$/",$nazwa)) {
				$nazwa_err = "*pole powinno się składać tylko z liter i cyfr";
				$valid_input = false;
			}
		}
		
		//miasto
		if (!empty($_POST[$fn_miasto])) {
			$miasto = test_input($_POST[$fn_miasto]);
			
			if (!preg_match("/^[a-zA-Z0-9 ]*$/",$miasto)) {
				$miasto_err = "*pole powinno się składać tylko z liter i cyfr";
				$valid_input = false;
			}
		}
	}

	echo "<h1 align='center'>Wyszukaj instytucje: </h1>" .
		"Id instytucji: <br><input type=text name='$fn_id_inst' value='$id_inst'>$id_inst_err<br>" .
		"Nazwa: <br><input type=text name='$fn_nazwa' value='$nazwa'>$nazwa_err<br>" .
		"Miasto: <br><input type=text name='$fn_miasto' value='$miasto'>$miasto_err<br>" .
		"<input type=submit value='Szukaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		$query = "SELECT $fn_id_inst, $fn_nazwa, $fn_miasto " .
			" FROM instytucje WHERE 1=1";
		
		add_cond_equal_to_query($fn_id_inst, $id_inst);
		add_cond_like_to_query($fn_nazwa, $nazwa);
		add_cond_like_to_query($fn_miasto, $miasto);
		$query = $query . " ORDER BY $fn_nazwa";
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		
		$result = pg_exec($link, $query);
		if (!$result) {
			echo pg_last_error($link);
		}

		printInst($result);

		pg_close($link);
	}
?>

<?php
	function printInst($result) {
		global $fn_id_inst;
		global $fn_nazwa;
		global $fn_miasto;
		
		echo ''.
		'<h2 align=center>Instytucje</h2>

		<table border="1" align=center>
		<tr>
		<th>Id instytucji</th>
		<th>Nazwa</th>
		<th>Miasto</th>
		</tr>';

		$numrows = pg_numrows($result);
		// Loop on rows in the result set.
		for($ri = 0; $ri < $numrows; $ri++) {
			echo "<tr>\n";
			$row = pg_fetch_array($result, $ri);
			echo "<td>" . $row[$fn_id_inst] . "</td> 
				<td>" . $row[$fn_nazwa] . "</td>
				<td>" . $row[$fn_miasto] . "</td>
				</tr>";
		}
		
		echo '</table>';
	}
?>



</body>
</html>
