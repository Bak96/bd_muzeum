<html>
<head>
<meta charset="utf-8"/>
<title>Wypożycz instytucji</title>
</head>
<body>
<?php
	session_start();
	
	if (!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}
?>

<?php include 'connect.php';?>
<?php include 'menu/menu.php';?>
<?php include 'vars.php';?>
<?php include 'helpers.php';?>

<?php
//ini_set('display_errors', 'On');
//error_reporting(E_ALL | E_STRICT);
?>


<form action="instytucja_lend.php" method="post">
<?php

	$valid_input = true;
	
	$id_inst = $id_eksp = $data_od = $data_do ='';
	$id_inst_err = $id_eksp_err = $data_od_err = $data_do_err = '';
	$query = '';
	
	if($_SERVER["REQUEST_METHOD"] == "POST") {
		//id_inst
		if (empty($_POST[$fn_id_inst])) {
			$id_inst_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$id_inst = test_input($_POST[$fn_id_inst]);
			
			if (!preg_match("/^[1-9][0-9]*$/",$id_inst)) {
				$id_inst_err = "*pole powinno składać się z samych cyfr";
				$valid_input = false;
			}
		}

		//id_eksp
		if (empty($_POST[$fn_id_eksp])) {
			$id_eksp_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$id_eksp = test_input($_POST[$fn_id_eksp]);
			
			if (!preg_match("/^[1-9][0-9]*$/", $id_eksp)) {
				$id_eksp_err = "*pole powinno składać się z samych cyfr";
				$valid_input = false;
			}
		}
		
		
		//data_od
		if (empty($_POST[$fn_data_od])) {
			$data_od_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$data_od = test_input($_POST[$fn_data_od]);
			
			if (!validate_date($data_od)) {
				$data_od_err = "*wprowadzono nieprawidłowy format";
				$valid_input = false;
			}
		}
		
		//data_do
		if (empty($_POST[$fn_data_do])) {
			$data_do_err = "*pole wymagane";
			$valid_input = false;
		}
		else {
			$data_do = test_input($_POST[$fn_data_do]);
			
			if (!validate_date($data_do)) {
				$data_do_err = "*wprowadzono nieprawidłowy format";
				$valid_input = false;
			}
		}
		
		if ($valid_input == true && !valid_date_interval($data_od, $data_do)) {
			$data_do_err = "*data do powinna byc pozniejsza niz data od";
			$valid_input = false;
		}
	}

	echo "<h1 align='center'>Wypożycz instytucji: </h1>" .
		"Id instytucji: <br><input type=text name='$fn_id_inst' value='$id_inst'>$id_inst_err<br>" .
		"Id eksponatu: <br><input type=text name='$fn_id_eksp' value='$id_eksp'>$id_eksp_err<br>" .
		"Data od:(DD-MM-YYYY) <br><input type=text name='$fn_data_od' value='$data_od'>$data_od_err<br>" .
		"Data do:(DD-MM-YYYY) <br><input type=text name='$fn_data_do' value='$data_do'>$data_do_err<br>" .
		"<input type=submit value='Dodaj'>";
?>
</form>

<?php
	if($_SERVER["REQUEST_METHOD"] == "POST" && $valid_input == true) {
		$query = "INSERT INTO ekspwinst($fn_id_inst, $fn_data_od, $fn_data_do, $fn_id_eksp)" .
			" VALUES('$id_inst', to_date('$data_od','DD-MM-YYYY'), to_date('$data_do','DD-MM-YYYY'), '$id_eksp')";
			
		$link = @pg_connect("host=$host port=$db_port dbname=$db_name user=$db_user password=$db_password");
		if (!$link) {
			echo "ERROR: nie udało się połączyć z bazą danych";
			exit();
		}
		
		$result = pg_exec($link, $query);
		
		if ($result) {
			echo "Rekord dodany pomyślnie";
		}
		else {
			echo "Nie udalo się dodać rekordu<br>";
			echo pg_last_error($link);
		}
		pg_close($link);
	}
?>

</body>
</html>
